This plugin will save stats to a BoltDB database and can be viewed from `/_caddy_stats`

Caddyfile: (Subject to change)
```
stats {
    exclude /_internal
}
```